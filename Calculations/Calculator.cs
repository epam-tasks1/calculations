﻿using System;

namespace Calculations
{
    public static class Calculator
    {
        public static double GetSumOne(int n)
        {
            double sum = 0;
            for (double i = 1; i <= n; i++)
            {
                sum += 1 / i;
            }

            return sum;
        }

        public static double GetSumTwo(int n)
        {
            double sum = 0;
            short numberSing = -1;
            for (double i = 1; i <= n; i++)
            {
                numberSing *= -1;
                sum += numberSing / (i * (i + 1));
            }

            return sum;
        }

        public static double GetSumThree(int n)
        {
            double sum = 0;
            for (double i = 1; i <= n; i++)
            {
                sum += 1 / Math.Pow(i, 5);
            }

            return sum;
        }

        public static double GetSumFour(int n)
        {
            double sum = 0;
            for (double i = 1; i <= n; i++)
            {
                sum += 1 / (((2 * i) + 1) * ((2 * i) + 1));
            }

            return sum;
        }

        public static double GetProductOne(int n)
        {
            double productOfNumber = 1;
            for (double i = 1; i <= n; i++)
            {
                productOfNumber *= 1 + (1 / (i * i));
            }

            return productOfNumber;
        }

        public static double GetSumFive(int n)
        {
            double sum = 0;
            short numberSing = 1;
            for (double i = 1; i <= n; i++)
            {
                numberSing *= -1;
                sum += numberSing / ((2 * i) + 1);
            }

            return sum;
        }

        public static double GetSumSix(int n)
        {
            double sum = 0;
            double factorial = 1;
            double divisor = 0;
            for (double i = 1; i <= n; i++)
            {
                factorial *= i;
                divisor += 1 / i;
                sum += factorial / divisor;
            }

            return sum;
        }

        public static double GetSumSeven(int n)
        {
            double count = Math.Sqrt(2);
            for (double i = 1; i < n; i++)
            {
                count = Math.Sqrt(2 + count);
            }

            return count;
        }

        public static double GetSumEight(int n)
        {
            double sum = 0;
            double sumOfSinuses = 0;
            for (double i = 1; i <= n; i++)
            {
                sumOfSinuses += Math.Sin(i * Math.PI / 180);
                sum += 1 / sumOfSinuses;
            }

            return sum;
        }
    }
}
